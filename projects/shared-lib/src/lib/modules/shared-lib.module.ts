import { NgModule } from '@angular/core';
import { SharedLibComponent } from '../components/shared-lib.component';



@NgModule({
  declarations: [
    SharedLibComponent
  ],
  imports: [
  ],
  exports: [
    SharedLibComponent
  ]
})
export class SharedLibModule { }
