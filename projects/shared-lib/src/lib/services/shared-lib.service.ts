import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedLibService {

  data = new BehaviorSubject<any>(null);

  constructor() {
    console.log('SharedLibService init');
  }
}
