/*
 * Public API Surface of shared-lib
 */

export * from './lib/services/shared-lib.service';
export * from './lib/components/shared-lib.component';
export * from './lib/modules/shared-lib.module';
