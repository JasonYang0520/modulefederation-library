## init
建立空的 angular application
```shell
ng new shared-lib --create-application=false
```

建立 shared library
```shell
ng g library shared-lib
```


## 打包
build package
```shell
npm build
```
包成 .tgz
```shell
cd .\dist\shared-lib\
npm pack
```


